// if it goes to one direction, prevent from stopping if 


window.onload = function() {

    //prepare canvas
    var canvas = document.getElementById('my_canvas');
    var ctx = canvas.getContext('2d');

    ctx.fillStyle = '#4A4E69';
    ctx.fillRect(0, 0, 800, 600);

    //initialize variables
    var x = 800 / 2;
    var y = 500;
    var rect_side = 25;
    var snake = [{
        x_coor: (canvas.width - rect_side) / 2,
        y_coor: (canvas.height - rect_side) / 2
    }];
    var food;
    var state = "left";
    var fps = 5;

    var req;
    //main function that redraws the snake
    function draw() {
        setTimeout(function() {

            ctx.clearRect(0, 0, 800, 600);
            ctx.fillStyle = '#4A4E69';
            ctx.fillRect(0, 0, 800, 600);
            draw_snake();
            req = window.requestAnimationFrame(draw);

        }, 1000 / fps);
    }

    draw();

    place_random_food();

    //add event listeners
    window.addEventListener("keydown", key_down, false);


    //function that calls draw_snake function with an argument that corresponds to the key pressed
    function key_down(e) {


        if (e.keyCode == 37) {
            if (state != "right" || snake.length == 1) {
                state = "left"
            }
        } else if (e.keyCode == 38) {
            if (state != "down" || snake.length == 1) {
                state = "up"
            }
        } else if (e.keyCode == 39) {
            if (state != "left" || snake.length == 1) {
                state = "right"
            }
        } else if (e.keyCode == 40) {
            if (state != "up" || snake.length == 1) {
                state = "down"
            }
        }
    };

    //function that draws snake on canvas when a key(up, down, left, or right arrow) is pressed
    function draw_snake() {

        var temp_x = x;
        var temp_y = y;
        if (state == "left") {
            temp_x = x - rect_side;
        } else if (state == "up") {
            temp_y = y - rect_side;
        } else if (state == "right") {
            temp_x = x + rect_side;
        } else if (state == "down") {
            temp_y = y + rect_side;
        }

        //check if there is collision
        if (no_collision(temp_x, temp_y)) {

            x = temp_x;
            y = temp_y;

            snake.push({
                x_coor: x,
                y_coor: y
            });

            if (found_food(x, y)) {
                fps += 0.3;
                place_random_food();
            } else {
                snake.shift();
            }

        } else {
            game_over();
        }


        draw_rect(food.x_coor, food.y_coor);

        for (i = 0; i < snake.length; i++) {
            draw_rect(snake[i].x_coor, snake[i].y_coor);
        }



    }

    //place a random food on canvas
    function place_random_food() {

        while (true) {
            var rand_x = Math.floor(Math.random() * canvas.width / rect_side) * rect_side;
            var rand_y = Math.floor(Math.random() * canvas.height / rect_side) * rect_side;

            if (no_collision(rand_x, rand_y)) {
                food = {
                    x_coor: rand_x,
                    y_coor: rand_y
                }
                draw_rect(food.x_coor, food.y_coor);
                break;
            }
        }
    }

    //function that draws rects of which the snake is made
    function draw_rect(x, y) {
        ctx.beginPath();
        ctx.fillStyle = '#9A8C98'
        ctx.strokeStyle = "#9A8C98";
        ctx.strokeRect(x, y, rect_side, rect_side);
        ctx.stroke();
        ctx.closePath();
    }

    //function that checks if the snake collided with itself or the walls
    function no_collision(head_x, head_y) {
        for (i = 0; i < snake.length; i++) {
            if ((snake[i].x_coor == head_x && snake[i].y_coor == head_y) || (head_x < 0 || head_x >= 800) || (head_y >= 600 || head_y < 0)) {
                return 0;
            }
        }
        return 1;
    }

    //function that checks if food is found
    function found_food(head_x, head_y) {
        if (head_x == food.x_coor && head_y == food.y_coor) {
            return 1;
        }
        return 0;
    }

    function game_over() {
        window.removeEventListener("keydown", key_down, false);

        var x = canvas.width / 2;
        var y = canvas.height / 2 + 40;

        ctx.font = '80pt Geo, sans-serif';
        ctx.textAlign = 'center';
        ctx.fillStyle = '#9A8C98';
        ctx.fillText('Game Over', x, y);
        ctx.fillStyle = "black";


    }


};