var express = require("express"),
    app = express();


//app config
app.set("view engine", "ejs");
app.use(express.static("public"));


//routes

app.get("/", function(req, res) {
    res.render("breakout");
})



app.listen(3000, () => console.log("server is listening"));